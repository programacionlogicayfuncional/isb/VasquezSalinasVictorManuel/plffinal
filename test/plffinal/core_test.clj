(ns plffinal.core-test
  (:require [clojure.test :refer :all]
            [plffinal.core :refer [izquierdo regresa-al-punto-de-origen? regresan-al-punto-de-origen?2 regreso-al-punto-de-origen mismo-punto-final? inverso diferenciax diferenciay
                                   contadorx contadory calcularxoy coordenadas coincidir coincidencias]]))


(deftest izquierdotest
  (testing "probrando funcion de conteo de pasos izquierdos"
    (is (= 5 (izquierdo ">>><<<><><")))))

(deftest vueltaorigen
  (testing "probando funcion de retorno al origen"
    (is (true? (regresa-al-punto-de-origen? "")))
    (is (true? (regresa-al-punto-de-origen? [])))
    (is (true? (regresa-al-punto-de-origen? (list))))
    (is (true? (regresa-al-punto-de-origen? "><")))
    (is (true? (regresa-al-punto-de-origen? (list \> \<))))
    (is (true? (regresa-al-punto-de-origen? "v^")))
    (is (true? (regresa-al-punto-de-origen? [\v \^])))
    (is (true? (regresa-al-punto-de-origen? "^>v<")))
     (is (true? (regresa-al-punto-de-origen? (list \^ \> \v \<))))
     (is (true? (regresa-al-punto-de-origen? "<<vv>>^^")))
     (is (true? (regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^])))
     (is (false? (regresa-al-punto-de-origen? ">")))
     (is (false? (regresa-al-punto-de-origen? (list \>))))
     (is (false? (regresa-al-punto-de-origen? "<^")))
     (is (false? (regresa-al-punto-de-origen? [\< \^])))
     (is (false? (regresa-al-punto-de-origen? ">>><<")))
     (is (false? (regresa-al-punto-de-origen? (list \> \> \> \< \<))))
     (is (false? (regresa-al-punto-de-origen? [\v \v \^ \^ \^])))))


(deftest problema2
  (testing "Test para ejercicios del problema 2"
    (is (true? (regresan-al-punto-de-origen?2 [])))
    (is (true? (regresan-al-punto-de-origen?2 "")))
    (is (true? (regresan-al-punto-de-origen?2 '([] "" (list)))))
    (is (true? (regresan-al-punto-de-origen?2 '("" "" "" "" [] [] [] (list) ""))))
    (is (true? (regresan-al-punto-de-origen?2 '(">><<" [\< \< \> \>] (list \^ \^ \v \v)))))

    (is (false? (regresan-al-punto-de-origen?2 '((list \< \>) "^^" [\> \<]))))
    (is (false? (regresan-al-punto-de-origen?2 '(">>>" "^vv^" "<<>>"))))
    (is (false? (regresan-al-punto-de-origen?2 '([\< \< \> \> \> \> \> \> \> \>]))))))


(deftest inversotest
  (testing "Probando funcion de retorno inverso"
    (is (= \< (inverso \>)))
    (is (= \> (inverso \<)))
    (is (= \^ (inverso \v)))))
(is (= \v (inverso \^)))

(deftest problema3
  (testing "Test para ejercicios del problema 3"
    (is (= '() (regreso-al-punto-de-origen "")))
    (is (= '() (regreso-al-punto-de-origen (list \^ \^ \^ \> \< \v \v \v))))
    (is (= '(\< \< \<) (regreso-al-punto-de-origen ">>>")))
    (is (= '(\< \< \^ \^ \^ \>) (regreso-al-punto-de-origen [\< \v \v \v \> \>])))))

(deftest testdifx
  (testing "Probando funcion de diferencia en coordenada en x"
    (is (= 2 (diferenciax "<<<>")))
    (is (= 2 (diferenciax [\< \< \< \>])))
    (is (= 2 (diferenciax '(\< \< \< \>))))))

(deftest testdify
  (testing "Probando funcion de diferencia en coordenada en x"
    (is (= 3 (diferenciay "^^^^v")))
    (is (= 3 (diferenciay [\^ \^ \^ \^ \v])))
    (is (= 3 (diferenciay '(\^ \^ \^ \^ \v))))))


(deftest problema4
  (testing "Test para ejercicios del problema 3"
    (is (true? (mismo-punto-final? "" [])))
    (is (true? (mismo-punto-final? "^^^" "<^^^>")))
    (is (true? (mismo-punto-final? [\< \< \< \>] (list \< \<))))
    (is (true? (mismo-punto-final? (list \< \v \>) (list \> \v \<))))
    (is (false? (mismo-punto-final? "" "<")))
    (is (false? (mismo-punto-final? [\> \>] "<>")))
    (is (false? (mismo-punto-final? [\> \> \>] [\> \> \> \>])))
    (is (false? (mismo-punto-final? (list) (list \^))))))


(deftest coxtest
  (testing "Probando funcion de conteo de coordenadas en x"
    (is (= '(1 0 1 0 1 0 -1) (contadorx ">^>v>v<")))
    (is (= '(1 0 1 0 1 0 -1) (contadorx [\> \^ \> \v \> \v \<])))
    (is (= '(1 0 1 0 1 0 -1) (contadorx '(\> \^ \> \v \> \v \<))))))


(deftest coytest
  (testing "Probando funcion de conteo de coordenadas en y"
    (is (= '(0 1 0 -1 0 -1 0) (contadory ">^>v>v<")))
    (is (= '(0 1 0 -1 0 -1 0) (contadory [\> \^ \> \v \> \v \<])))
    (is (= '(0 1 0 -1 0 -1 0) (contadory '(\> \^ \> \v \> \v \<))))))


(deftest calculacoordtest
  (testing "Probando funcion de conteo de coordenadas en x"
    (is (= '(1 1 2 2 3 3 2) (calcularxoy (contadorx ">^>v>v<"))))
    (is (= '(1 1 2 2 3 3 2) (calcularxoy (contadorx [\> \^ \> \v \> \v \<]))))
    (is (= '(1 1 2 2 3 3 2) (calcularxoy (contadorx '(\> \^ \> \v \> \v \<)))))
    (is (= '(0 1 1 0 0 -1 -1) (calcularxoy (contadory ">^>v>v<"))))
    (is (= '(0 1 1 0 0 -1 -1) (calcularxoy (contadory [\> \^ \> \v \> \v \<]))))
    (is (= '(0 1 1 0 0 -1 -1) (calcularxoy (contadory '(\> \^ \> \v \> \v \<)))))))

(deftest unircoordtest
  (testing "probando funcion para unir las coordenadas"
    (is (= '((1 0) (1 1) (2 1) (2 0) (3 0) (3 -1) (2 -1)) (coordenadas (calcularxoy (contadorx ">^>v>v<")) (calcularxoy (contadory ">^>v>v<")))))
    (is (= '((1 0) (1 1) (2 1) (2 0) (3 0) (3 -1) (2 -1)) (coordenadas (calcularxoy (contadorx [\> \^ \> \v \> \v \<])) (calcularxoy (contadory [\> \^ \> \v \> \v \<])))))))

(deftest buscarcoin
  (testing "probando funcion para buscar coincidencias con colecciones introducidas de manera manual"
    (is (= 2 (coincidir '((0 1) (1 1) (2 1) (3 1) (3 2)) '((1 0) (1 1) (1 2) (0 2)))))))

(deftest buscarcoincidencias
  (testing "buscar el numero de coincidencias de dos secuencias de pasos"
    (is (= 1 (coincidencias (list \< \<) [\> \>])))
    (is (= 2 (coincidencias [\^ \> \> \> \^] ">^^<")))
    (is (= 1 (coincidencias "<<vv>>^>>" "vv<^")))
    (is (= 6 (coincidencias ">>>>>" [\> \> \> \> \>])))
    (is (= 6 (coincidencias [\> \> \> \> \>] (list \> \> \> \> \> \> \^ \^ \^ \^))))))


























































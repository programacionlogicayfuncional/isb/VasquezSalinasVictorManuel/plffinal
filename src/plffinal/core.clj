(ns plffinal.core
  (:require [clojure.math.numeric-tower]))


;Problema 1

(defn izquierdo 
  [sentidos]
 (count (filter true? (map (fn [caracter] (= \< caracter)) sentidos))))

(izquierdo [\> \> \> \< \< \<])


(defn derecho
  [sentidos]
  (count (filter true? (map (fn [caracter] (= \> caracter)) sentidos))))


(defn arriba
  [sentidos]
  (count (filter true? (map (fn [caracter] (= \^ caracter)) sentidos))))


(defn abajo
  [sentidos]
  (count (filter true? (map (fn [caracter] (= \v caracter)) sentidos))))

(defn regresa-al-punto-de-origen?
  [sentidos]
  (if (and (= (izquierdo sentidos) ( derecho sentidos)) (= (arriba sentidos) (abajo sentidos))) true false))

(regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^])







;Problema 2

(defn regresan-al-punto-de-origen?2
  [colecciones]
  (if (= (count (filter true? (map regresa-al-punto-de-origen? colecciones)))
         (count colecciones)) true false))

(regresan-al-punto-de-origen?2 '(">><<" [\< \< \> \>] (list \^ \^ \v \v)))



;Problema 3

(defn inverso
  [caracter]
  (cond
    (= \< caracter) \>
    (= \> caracter) \<
    (= \^ caracter) \v
    (= \v caracter) \^
    :else ""))


(defn regreso-al-punto-de-origen
  [secuencia]
  (if (true? (regresa-al-punto-de-origen? secuencia)) '() (map inverso (reverse secuencia))))

(regreso-al-punto-de-origen [\< \v \v \v \> \>])
                                              













;Problema 4

;Calcular diferencias de avances en eje X
(defn diferenciax
  [secuencia]
  (clojure.math.numeric-tower/abs (- (izquierdo secuencia) (derecho secuencia))))

;Calcular diferencias de avances en eje Y
(defn diferenciay
  [secuencia]
  (clojure.math.numeric-tower/abs (- (arriba secuencia) (abajo secuencia))))

;Comparar avance real en eje X y eje Y en ambas secuencias
(defn mismo-punto-final?
  [seq1 seq2]
  (if (and (= (diferenciax seq1) (diferenciax seq2)) (= (diferenciay seq1) (diferenciay seq2)))
    true false))

(mismo-punto-final? (list \< \v \>) (list \> \v \<))




















;Problema 5
;Calcular avances en eje X y en eje Y
(defn contadorx
  [secuencia]
  (map (fn [valor] (cond (= \> valor) 1 (= \< valor) -1 :else 0)) secuencia))

(contadorx [\> \> \> \< \< \<])

(defn contadory
  [secuencia]
  (map (fn [valor] (cond (= \^ valor) 1 (= \v valor) -1 :else 0)) secuencia))

(contadory [\> \> \^ \< \v \^])

;Calcular coordenada en eje X y en eje Y
(defn calcularxoy
  [seq]
  (letfn [(f [secuencia acc] (if (empty? secuencia) acc
                                 (f (rest secuencia) (concat acc (vector (+ (last acc) (first secuencia)))))))]
    (f (rest seq)(vector (first seq)))))

(calcularxoy (contadorx [\> \^ \> \v \> \v \<]))
(calcularxoy (contadory [\> \^ \> \v \> \v \<])) 

;Combinar coordenadas de eje X y eje Y 
(defn coordenadas 
  [se1 se2]
  (letfn [(f [seq1 seq2 acc] (if (empty? seq1) acc 
                                 (f (rest seq1) (rest seq2) (concat acc (vector (first seq1) (first seq2))))))]
   (partition-all 2 (f se1 se2 []))))

(coordenadas (calcularxoy (contadorx [\> \^ \> \v \> \v \<])) (calcularxoy (contadory [\> \^ \> \v \> \v \<])))


;Buscar coincidencias de coordenadas en ambas secuencias
(defn coincidir
  [se1 se2]
  (letfn [(f [seq1 seq2 acc] (if (or (zero? (count seq1)) (zero? (count seq2)))acc 
                                 (f (rest seq1) (rest seq2) (+ acc (if (= (first seq1) (first seq2)) 1 0)))))]
    (f se1 se2 1)))


(defn coincidencias
  [seq1 seq2]
  (if (or (zero? (count seq1)) (zero? (count seq2))) 1 (coincidir (coordenadas (calcularxoy (contadorx seq1)) (calcularxoy (contadory seq1)))
             (coordenadas (calcularxoy (contadorx seq2)) (calcularxoy (contadory seq2))))))

(coincidencias [\> \> \> \> \>] (list \> \> \> \> \> \> \^ \^ \^ \^))